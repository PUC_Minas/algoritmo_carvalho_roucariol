ECHO TRABALHO DE COMPUTACAO DISTRIBUIDA
ECHO Implementacao do algoritmo de Carvalho e Roucariol

SET CLIENTES=2
SET PORTA_INI=777

ECHO Quantidade de clientes escolhida: %CLIENTES%

ECHO Compilando cliente
CD Cliente
javac -encoding utf8 -d . *.java
jar cvfe ..\Cliente.jar tp.No tp/*.class

ECHO Compilando servidor
CD ..\Servidor
javac -encoding utf8 -d . *.java
jar cvfe ..\Servidor.jar servidorimpressao.ServidorImpressao servidorimpressao/*.class

ECHO Inicializando servidor
CD ..
START cmd.exe /k java -jar Servidor.jar

SET /a "MAX_CLIENTES=%CLIENTES%-1"
@ECHO OFF
BREAK > hosts.txt
FOR /L %%A IN (0,1,%MAX_CLIENTES%) DO (
  ECHO localhost:%PORTA_INI%%%A >> hosts.txt
)

ECHO Inicializando (%CLIENTES%) Clientes
FOR /L %%A IN (0,1,%MAX_CLIENTES%) DO (
  START cmd.exe /k java -jar Cliente.jar %PORTA_INI%%%A %%A
)