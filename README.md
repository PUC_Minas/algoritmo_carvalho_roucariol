# Exclusão mútua em sistemas distribuídos, utilizando o Algoritmo de Carvalho e Roucariol


**Progresso:** CONCLUIDO<br />
**Autor:** Paulo Victor de Oliveira Leal<br />
**Data:** 2018<br />

### Objetivo
Implementar o algoritmo de exclusão mútua de Carvalho e Roucariol. A exclusão mútua deve restringir o acesso de um recurso compartilhado à apenas um processo por vez.

### Observação

IDE:  [Visual Studio Code](https://code.visualstudio.com/)<br />
Linguagem: [JAVA 8](https://php.net/)<br />
Banco de dados: Não utiliza<br />

### Execução

    Edite o script start, colocando a quantidade de clientes para executar
    $ .\start
    



### Contribuição

Esse projeto está concluido e é livre para o uso.

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->